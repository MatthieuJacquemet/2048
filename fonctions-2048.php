<?php

function write_log($log) {
    file_put_contents("jeu-2048.log", $log."\n", FILE_APPEND);
}

function score_vers_fichier() {
    global $score;
    file_put_contents("score.txt", $score);
}

function fichier_vers_score() {
    global $score;
    $score = (int) file_get_contents("score.txt");
}

function meilleur_score_vers_fichier() {
    global $meilleur_score;
    file_put_contents("meilleur-score.txt", $meilleur_score);
}

function fichier_vers_meilleur_score() {
    global $meilleur_score;
    $meilleur_score = (int) file_get_contents("meilleur-score.txt");
}

function matrice_vers_fichier() {
    global $grille;
    $lines=[];
    foreach ($grille as $i => $line){
        $lines[$i] = implode(" ", $line);}
    file_put_contents("grille.txt", implode("\n", $lines));
}

function fichier_vers_matrice() {
    global $grille;
    $lines = explode("\n", file_get_contents("grille.txt"));
    foreach ($lines as $i => $line)
        $grille[$i] = explode(" ", $line);
}

function nouvelle_partie(){
    global $grille, $taille;
    $grille = array_fill(0, $taille,
        array_fill(0, $taille, 0));
    place_nouveau_nb();
    place_nouveau_nb();
    $score = 0;
}

function tirage_position_vide() {
    global $taille, $grille;
    do {
        $x = rand(0, $taille - 1);
        $y = rand(0, $taille - 1);
    } while ($grille[$x][$y] != 0);
    return [$x, $y];
}

function grille_pleine() {
    global $grille;
    foreach ($grille as $line) {
        foreach ($line as $case)
            if ($case == 0) return false;}
    return true;
}

function tirage_2ou4() {
    return rand(1,2)*2;
}

function place_nouveau_nb() {
    global $grille;
    $coord = tirage_position_vide();
    $grille[$coord[0]][$coord[1]] = tirage_2ou4();
}

function decale_ligne_gauche($l) {
    global $grille, $taille;
    $ligne = array_fill(0,$taille,0);
    $i = 0;
    for ($j=0; $j<$taille; $j++) {
        if ($grille[$l][$j] != 0){
            $ligne[$i] = $grille[$l][$j];
            $i++;
        }
    }
    $grille[$l] = $ligne;
}

function decale_ligne_droite($l) {
    global $grille, $taille;
    $ligne = array_fill(0,$taille,0);
    $i = $taille-1;
    for ($j=$taille-1; $j>=0; $j--) {
        if ($grille[$l][$j] != 0){
            $ligne[$i] = $grille[$l][$j];
            $i--;
        }
    }
    $grille[$l] = $ligne;
}

function decale_col_haut($c) {
    global $grille, $taille;
    $col = array_fill(0,$taille,0);
    $i = 0;
    for ($j=0; $j<$taille; $j++) {
        if ($grille[$j][$c] != 0){
            $col[$i] = $grille[$j][$c];
            $i++;
        }
    }
    for ($i=0; $i<$taille; $i++)
        $grille[$i][$c] = $col[$i];
}

function decale_col_bas($c) {
    global $grille, $taille;
    $col = array_fill(0,$taille,0);
    $i = $taille-1;
    for ($j=$taille-1; $j>=0; $j--) {
        if ($grille[$j][$c] != 0){
            $col[$i] = $grille[$j][$c];
            $i--;
        }
    }
    for ($i=$taille-1; $i>=0; $i--)
        $grille[$i][$c] = $col[$i];
}

function fusion_ligne_gauche($l) {
    global $grille, $taille, $score;
    for ($i=0; $i<$taille; $i++){
        if ($grille[$l][$i] == $grille[$l][$i+1]){
            $grille[$l][$i] *= 2;
            $score += $grille[$l][$i];
            $grille[$l][$i+1] = 0;
            $i++;
        }
    }
}

function fusion_ligne_droite($l) {
    global $grille, $taille, $score;
    for ($i=$taille-1; $i>0; $i--){
        if ($grille[$l][$i] == $grille[$l][$i-1]){
            $grille[$l][$i] *= 2;
            $score += $grille[$l][$i];
            $grille[$l][$i-1] = 0;
            $i--;
        }
    }	
}

function fusion_col_haut($c) {
    global $grille, $taille, $score;
    for ($i=0; $i<$taille-1; $i++){
        if ($grille[$i][$c] == $grille[$i+1][$c]){
            $grille[$i][$c] *= 2;
            $score += $grille[$i][$c];
            $grille[$i+1][$c] = 0;
            $i++;
        }
    }
}

function fusion_col_bas($c) {
    global $grille, $taille, $score;
    for ($i=$taille-1; $i>0; $i--){
        if ($grille[$i][$c] == $grille[$i-1][$c]){
            $grille[$i][$c] *= 2;
            $score += $grille[$i][$c];
            $grille[$i-1][$c] = 0;
            $i--;
        }
    }
}

function affiche_case($i, $j) {
    global $grille;
    echo '<td class="c'.min($grille[$i][$j],2048).'">';
    if ($grille[$i][$j])
        echo $grille[$i][$j];
    echo '</td>';
}

function action_gauche() {
    global $grille, $taille;
    $tab = array_fill(0,$taille,0);
    for ($i=0; $i<$taille; $i++){
        for ($j=0; $j<$taille; $j++)
            $tab[$j] = $grille[$i][$j];
        $tab = decalage($tab);
        $tab = fusion($tab);
        $tab = decalage($tab);
        for ($j=0; $j<$taille; $j++)
            $grille[$i][$j] = $tab[$j];
    }
}

function action_droite() {
    global $grille, $taille;
    $tab = array_fill(0,$taille,0);
    for ($i=0; $i<$taille; $i++){
        for ($j=0; $j<$taille; $j++)
            $tab[$j] = $grille[$i][$taille-1-$j];
        $tab = decalage($tab);
        $tab = fusion($tab);
        $tab = decalage($tab);
        for ($j=0; $j<$taille; $j++)
            $grille[$i][$taille-1-$j] = $tab[$j];
    }
}

function action_bas() {
    global $grille, $taille;
    $tab = array_fill(0,$taille,0);
    for ($i=0; $i<$taille; $i++){
        for ($j=0; $j<$taille; $j++)
            $tab[$j] = $grille[$taille-1-$j][$i];
        $tab = decalage($tab);
        $tab = fusion($tab);
        $tab = decalage($tab);
        for ($j=0; $j<$taille; $j++)
            $grille[$taille-1-$j][$i] = $tab[$j]; 
    }
}

function action_haut() {
    global $grille, $taille;
    $tab = array_fill(0,$taille,0);
    for ($i=0; $i<$taille; $i++){
        for ($j=0; $j<$taille; $j++)
            $tab[$j] = $grille[$j][$i];
        $tab = decalage($tab);
        $tab = fusion($tab);
        $tab = decalage($tab);
        for ($j=0; $j<$taille; $j++)
            $grille[$j][$i] = $tab[$j]; 
    }
}

function decalage($tab) {
    global $taille;
    $ret = array_fill(0,$taille,0);
    $i = 0;
    for ($j=0; $j<$taille; $j++) {
        if ($tab[$j] != 0){
            $ret[$i] = $tab[$j];
            $i++;
        }
    }
    return $ret;
}

function fusion($tab) {
    global $taille, $score;
    for ($i=0; $i<$taille-1; $i++){
        if ($tab[$i] == $tab[$i+1]){
            $tab[$i] *= 2;
            $score += $tab[$i];
            $tab[$i+1] = 0;
            $i++;
        }
    }
    return $tab;
}

?>