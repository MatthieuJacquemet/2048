#!/usr/bin/env bash

echo "entrer le chemin vers le dossier source du site :"
while read SITE_SOURCE ; do
	if [ -d "$SITE_SOURCE" ]; then
		break
	fi
	echo "le chemin n'existe pas, ressayez :"
done

SITE_NAME=(basename "$SITE_SOURCE")
SITE_CONF="/etc/apache2/sites-available/$SITE_NAME.conf"

set -x

# sudo apt install chromium-browser
# sudo apt install openssh-server
# sudo apt install apache2
# sudo apt install libapache2-mod-php

# sudo touch "$SITE_CONF"
# sudo echo "Alias \"/$SITE_NAME\" \"$SITE_SOURCE\"" >"$SITE_CONF"
# sudo ufw allow 'Apache'
# sudo a2ensite "$SITE_CONF"

# sudo systemctl start ssh

ssh-keygen -f ~/.ssh/id_rsa -N ""
cat ~/.ssh/id_rsa >>~/.ssh/authorized_keys
chmod go-rwx ~/.ssh/authorized_keys

pip3 install psutil
pip3 install pychrome
pip3 install pyinotify
pip3 install git+https://github.com/Pithikos/python-websocket-server

UTILS="$HOME/Utils/python"

mkdir -p $UTILS
cp utils.py $UTILS
cp utils_types.py $UTILS
cp client.py $UTILS
# cp live_reload.js "$SITE_SOURCE"
# cp -r .vscode "$SITE_SOURCE"

chmod +x $UTILS/client.py

CODE='
if [ -d "$HOME/Utils/python" ] ; then
	PATH="$HOME/Utils/python:$PATH"
fi\n
\n
if [ -d "$HOME/Utils/python" ] ; then
	PYTHONPATH="$HOME/Utils/python:$PYTHONPATH"
fi
export PYTHONPATH'

if [ ! cat ~/.profile | grep "$CODE" ]; then
	echo "$CODE" >>~/.profile
fi

set +x

echo "deconnectez vous puis reconnectez vous"
