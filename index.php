<?php
    global $score, $grille, $taille, $meilleur_score;
	$taille = 4;

	require_once('fonctions-2048.php');

	fichier_vers_score();
	fichier_vers_matrice();

	if (isset($_GET["action-joueur"])) {
		$action = $_GET["action-joueur"];
		if ($action == "reset" || $action == "" ||
			$action == "Fin du jeu!") {
			nouvelle_partie();
		} else {
			$tmp_grille = $grille;
			$tmp_score = $score;
			switch ($action) {
				case "up" : action_haut(); break;
				case "down" : action_bas(); break;
				case "left" : action_gauche(); break;
				case "right" : action_droite(); break;
			}
			if ($grille != $tmp_grille)
				place_nouveau_nb();
		}
	}

	fichier_vers_meilleur_score();
	if ($score > $meilleur_score){
		$meilleur_score = $score;
		meilleur_score_vers_fichier();
	}

	score_vers_fichier();
	matrice_vers_fichier();
?>
<!doctype html> 
<html> 
	<head>
	 	<meta charset="utf-8" />					
		<title>2048</title>
		<link rel="stylesheet" href="style.css" />
	</head>
	<body>
		<div class="container">
			<div class="header">
				<h1>2048</h1>
				<div class="scores">
					<div class="score-title">
						<div>score</div><?php echo $score."\n"; ?>
					</div>
					<div class="score-title">
						<div>meilleur</div><?php echo $meilleur_score."\n"; ?>
					</div>
					<form id="reset-form" method="get" action="index.php">
						<input type="submit" name="action-joueur" value="reset"/>
					</form>
				</div>
			</div>
			<div class="table-container">
				<table class="grid-area" id="grille">
					<?php for ($i=0; $i<$taille; $i++){ ?>
						<tr>
        				<?php for ($j=0; $j<$taille; $j++)
            				affiche_case($i, $j); ?>
        				</tr>
					<?php } ?>
				</table>
				<?php if (grille_pleine() && $score == $tmp_score) { ?>
					<form id="overlay" method="get" action="index.php">
						<input class="grid-area" type="submit" name="action-joueur" value="Fin du jeu!">
					</form>
				<?php } ?>
			</div>
		</div>
		<form id="controls" method="get" action="index.php">
            <input type="submit" name="action-joueur" value="up"/>
			<input type="submit" name="action-joueur" value="down"/>
			<input type="submit" name="action-joueur" value="left"/>
			<input type="submit" name="action-joueur" value="right"/>
		</form>
		<p class="regles">Le but est d'atteindre 2048, vous pouvez deplacer les cases en bas, en haut a gauche et a droite.
			Si deux cases côte à côte on le même nombre, alors elle fusionnent pour donner la somme de ses deux nombres.
			Si il n'y a plus de cases libres, alors la partie est fini.
		</p>
	</body>
	<script type="text/javascript" src="live_reload.js"></script>
</html> 