#!/usr/bin/env bash
source ./.vars.sh

function find_window(){
    pids=$(xdotool search --class chromium)
    for pid in $pids; do
        name=$(xdotool getwindowname $pid)
        echo "$name"
        if [[ $name == "2048" ]]; then
            echo $pid
            return 0
        fi
    done
    return 1
}

find_window

# xdotool windowactivate ${gg}
# if [ -z "$(xdotool search --name 2048)" ] ; then
#     if [ $1 == "local" ]; then
#         echo ok
#         chromium-browser "$LOCAL"
#     elif [ $1 == "remote" ]; then
#         chromium-browser "$URL"
#     fi
# else
#     PREV=$(xdotool getactivewindow)
#     WID=$(xdotool search --onlyvisible --classname chromium)
#     xdotool windowactivate ${WID}
#     xdotool key Ctrl+R
#     xdotool windowactivate ${PREV}
# fi